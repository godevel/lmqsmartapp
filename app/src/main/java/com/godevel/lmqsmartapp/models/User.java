package com.godevel.lmqsmartapp.models;

import java.io.Serializable;
import java.util.Comparator;

public class User implements Serializable {

    public User() { }

    private String firstName;

    private String lastName;

    private String email;

    private String phone;

    private String notes;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getFullName () {
        return firstName + " " + lastName;
    }

    //comparators
    public static Comparator<User> nameComparator = new Comparator<User>() {

        public int compare(User s1, User s2) {
            String User1 = s1.getFullName().toUpperCase();
            String User2 = s2.getFullName().toUpperCase();

            //ascending order
            return User1.compareTo(User2);
        }};
}