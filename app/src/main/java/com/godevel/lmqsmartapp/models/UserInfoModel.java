package com.godevel.lmqsmartapp.models;

import com.godevel.lmqsmartapp.utils.Constants;
import com.godevel.lmqsmartapp.utils.DateUtils;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UserInfoModel implements Serializable {

    /* region properties */
    private User user;

    private List<Appointment> nextTreeVisits; //ordered ASC by date

    private List<Appointment> nextAppointmentsToNextRevision; //ordered ASC by date

    private List<Appointment> oldAppointments; //ordered ASC by date

    private AppointmentType nextRevision;

    private List<Certificate> certificates;

    private Order nextOrderToPay; //NOT USED (ORDERS for old payment info)

    private String paymentInfo;

    private List<Order> orders;

    private String revisionGoals;

    private String frequency;

    /* endregion properties */

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Appointment> getNextTreeVisits() {
        return nextTreeVisits;
    }

    public void setNextTreeVisits(List<Appointment> nextTreeVisits) {
        this.nextTreeVisits = nextTreeVisits;
    }

    public List<Appointment> getNextAppointmentsToNextRevision() {
        return nextAppointmentsToNextRevision;
    }

    public void setNextAppointmentsToNextRevision(List<Appointment> nextAppointmentsToNextRevision) {
        this.nextAppointmentsToNextRevision = nextAppointmentsToNextRevision;
    }

    public List<Appointment> getOldAppointments() {
        return oldAppointments;
    }

    public void setOldAppointments(List<Appointment> oldAppointments) {
        this.oldAppointments = oldAppointments;
    }

    public AppointmentType getNextRevision() {
        return nextRevision;
    }

    public void setNextRevision(AppointmentType nextRevision) {
        this.nextRevision = nextRevision;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void addCertificate(Certificate certificate) {
        if (certificates == null) {
            certificates = new ArrayList<>();
        }

        certificates.add(certificate);
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

    public int getNumberOfRemainingVisits() {
        int count = 0;
        if (nextAppointmentsToNextRevision != null) {
            count += nextAppointmentsToNextRevision.size();
        }

        if (certificates != null) {
            for (Certificate c: certificates) {
                if (c != null) {
                    for (Integer i : c.getAppointmentTypeIDs()) {
                        Integer ii = c.getRemainingCountsByAppointment(i.toString());
                        if (ii != null) {
                            count += ii.intValue();
                        }
                    }
                }
            }
        }

        return count;
    }

    public int getScheduledVisits() {
        return nextAppointmentsToNextRevision != null ? nextAppointmentsToNextRevision.size() : 0;
    }

    public int getNoScheduledVisits() {
        int count = 0;

        if (certificates != null) {
            for (Certificate c: certificates) {
                if (c != null) {
                    for (Integer i : c.getAppointmentTypeIDs()) {
                        Integer ii = c.getRemainingCountsByAppointment(i.toString());
                        if (ii != null) {
                            count += ii.intValue();
                        }
                    }
                }
            }
        }

        return count;
    }

    public Order getNextOrderToPay() {
        return nextOrderToPay;
    }

    public void setNextOrderToPay(Order nextOrderToPay) {
        this.nextOrderToPay = nextOrderToPay;
    }

    public String getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(String paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void addOrders(Order order) {
        if (this.orders == null) {
            this.orders = new ArrayList<>();
        }

        this.orders.add(order);
    }

    public String getRevisionGoals() {
        return revisionGoals;
    }

    public void setRevisionGoals(String revisionGoals) {
        this.revisionGoals = revisionGoals;
    }


    public String getFrequency() {
        return this.frequency;
    }

    public void setFrequency (String frequency) {
        this.frequency = frequency;
    }

    public String getFirstAdjustment() {
        for (int i = 0; i < oldAppointments.size(); i-- ) {
            Appointment appointment = oldAppointments.get(i);
            if (appointment.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()) {
                return appointment.getDate();
            }
        }

        return "";
    }

    public long getNumberDaysFromFirstAdjustment() throws ParseException {
        for (int i = 0; i < oldAppointments.size(); i++ ) {
            Appointment appointment = oldAppointments.get(i);
            if (appointment.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()) {
                Date date = appointment.getDatetimeFormatted();
                long diff = new Date().getTime() - date.getTime();
                return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            }
        }

        return 0;
    }

    public int getHistoryAdjustments() {
        return this.oldAppointments.size();
    }

    public long getNumberDaysFromLastAdjustment() throws ParseException {
        for (int i = oldAppointments.size() - 1; i >= 0; i-- ) {
            Appointment appointment = oldAppointments.get(i);
            if (appointment.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()) {
                Date date = appointment.getDatetimeFormatted();
                if (DateUtils.isToday(appointment.getDatetimeFormatted())) {
                    return 0;
                }
                long diff = new Date().getTime() - date.getTime();
                long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                long hour = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
                if (days == 0 && hour > 8) {
                    return 1;
                }
                return days;
            }
        }

        return -1;
    }
}
