package com.godevel.lmqsmartapp.models;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AppointmentType implements Serializable {
    private Integer id;

    private String name;

    private String date;

    private String time;

    private String endTime;

    private String datetime;

    public AppointmentType() {
    }

    public AppointmentType(Integer id) {
        this.id = id;
    }

    public AppointmentType(Integer id, String name, String date, String time, String endTime, String datetime) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.time = time;
        this.endTime = endTime;
        this.datetime = datetime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Date getDatetimeFormatted() throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        return df.parse(datetime);
    }

    public void setDatetime(Date datetime) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        this.datetime = df.format(datetime);
    }
}
