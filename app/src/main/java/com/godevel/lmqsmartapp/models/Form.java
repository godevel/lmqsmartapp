package com.godevel.lmqsmartapp.models;

import java.io.Serializable;
import java.util.List;

public class Form implements Serializable {

    private int id;

    private String name;

    private List<Field> values;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Field> getValues() {
        return values;
    }

    public void setValues(List<Field> values) {
        this.values = values;
    }
}
