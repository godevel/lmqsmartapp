package com.godevel.lmqsmartapp.models;

import com.godevel.lmqsmartapp.models.enums.PaymentInfo;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Order implements Serializable {

    private int id;

    private float total;

    private String time;

    private String firstName;

    private String lastName;

    private String notes;

    private PaymentInfo status;

    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotal() {
        return total;
    }

    public String getFormattedTotal() {
        return String.valueOf(this.total) + " €";
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Date getTimeFormatted() throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.parse(time);
    }

    public void setDatetime(Date datetime) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.time = df.format(datetime);
    }

    public String getTimeFormattedLiteral() throws ParseException {
        Date aux = getTimeFormatted();
        String date = "";
        if (aux != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(aux);
            date += cal.get(Calendar.DAY_OF_MONTH) + " ";
            date += this.getMonth(cal.get(Calendar.MONTH)) + " ";
            date += cal.get(Calendar.YEAR);
        }
        return date;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public PaymentInfo getStatus() {
        return status;
    }

    public void setStatus(PaymentInfo status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String getMonth(int month) {
        String result;
        switch(month){
            case 0:
            {
                result="Enero";
                break;
            }
            case 1:
            {
                result="Febrero";
                break;
            }
            case 2:
            {
                result="Marzo";
                break;
            }
            case 3:
            {
                result="Abril";
                break;
            }
            case 4:
            {
                result="Mayo";
                break;
            }
            case 5:
            {
                result="Junio";
                break;
            }
            case 6:
            {
                result="Julio";
                break;
            }
            case 7:
            {
                result="Agosto";
                break;
            }
            case 8:
            {
                result="Septiembre";
                break;
            }
            case 9:
            {
                result="Octubre";
                break;
            }
            case 10:
            {
                result="Noviembre";
                break;
            }
            case 11:
            {
                result="Diciembre";
                break;
            }
            default:
            {
                result="Error";
                break;
            }
        }
        return result;
    }
}
