package com.godevel.lmqsmartapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class SharedDataPref implements Parcelable {

    private List<Order> orders;

    protected SharedDataPref(Parcel in) {
        
    }

    public SharedDataPref() {

    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static final Creator<SharedDataPref> CREATOR = new Creator<SharedDataPref>() {
        @Override
        public SharedDataPref createFromParcel(Parcel in) {
            return new SharedDataPref(in);
        }

        @Override
        public SharedDataPref[] newArray(int size) {
            return new SharedDataPref[size];
        }
    };
}
