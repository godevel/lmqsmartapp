package com.godevel.lmqsmartapp.models;

import java.io.Serializable;
import java.util.AbstractMap;

public class Certificate implements Serializable {

    private int id;

    private String certificate;

    private int productID;

    private int orderID;

    private int [] appointmentTypeIDs;

    private String name;

    private String email;

    private String type;

    private Object remainingCounts;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int[] getAppointmentTypeIDs() {
        return appointmentTypeIDs;
    }

    public void setAppointmentTypeIDs(int[] appointmentTypeIDs) {
        this.appointmentTypeIDs = appointmentTypeIDs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getRemainingCounts() {
        return remainingCounts;
    }

    public void setRemainingCounts(Object remainingCounts) {
        this.remainingCounts = remainingCounts;
    }

    public Integer getRemainingCountsByAppointment(String propertyName) {
        AbstractMap<String, Double> ab = (AbstractMap) remainingCounts;
        if (ab == null) {
            return null;
        }
        Double b = ab.get(propertyName);
        if (b == null) {
            return null;
        }
        return  b.intValue();
    }
}
