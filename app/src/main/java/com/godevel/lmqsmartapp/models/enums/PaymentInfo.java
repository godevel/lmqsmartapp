package com.godevel.lmqsmartapp.models.enums;

import java.io.Serializable;

public enum PaymentInfo  implements Serializable {
    paid,
    unpaid
}
