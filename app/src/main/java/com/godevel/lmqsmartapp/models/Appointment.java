package com.godevel.lmqsmartapp.models;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Appointment implements Serializable {
    private Integer id;

    private String firstName;

    private String lastName;

    private String date;

    private String time;

    private String endTime;

    private String datetime;

    private float price;

    private float priceSold;

    private boolean paid;

    private float amountPaid;

    private String type; //service type

    private Integer appointmentTypeID;

    private String classID;

    private Integer duration;

    private String certificate;

    private List<Form> forms;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Date getDatetimeFormatted() throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        return df.parse(datetime);
    }

    public void setDatetime(Date datetime) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        this.datetime = df.format(datetime);
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPriceSold() {
        return priceSold;
    }

    public void setPriceSold(float priceSold) {
        this.priceSold = priceSold;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public float getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(float amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppointmentTypeID() {
        return appointmentTypeID;
    }

    public void setAppointmentTypeID(Integer appointmentTypeID) {
        this.appointmentTypeID = appointmentTypeID;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public List<Form> getForms() {
        return forms;
    }

    public void setForms(List<Form> forms) {
        this.forms = forms;
    }

    //comparators
    public static Comparator<Appointment> dateTimeComparator = new Comparator<Appointment>() {

        public int compare(Appointment s1, Appointment s2) {
            try {
                Date date1 = s1.getDatetimeFormatted();
                Date date2 = s2.getDatetimeFormatted();
                //ascending order
                return date1.compareTo(date2);
            } catch (ParseException e) {
                return 0;
            }
        }};
}
