package com.godevel.lmqsmartapp;

import android.annotation.SuppressLint;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.godevel.lmqsmartapp.enums.StatesNFC;
import com.godevel.lmqsmartapp.gateway.UserInfoGateway;
import com.godevel.lmqsmartapp.models.Order;
import com.godevel.lmqsmartapp.models.SharedDataPref;
import com.godevel.lmqsmartapp.models.UserInfoModel;
import com.godevel.lmqsmartapp.nfc.NFCManager;
import com.godevel.lmqsmartapp.utils.Constants;
import com.godevel.lmqsmartapp.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_WAITING_NFC;
import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_READING_NFC;
import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_BUILDING;
import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_CALL_RF;
import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_CALL_RF_2;

public class MainActivity extends AppCompatActivity {
    private boolean fromThisActivity = true;
    //NFC
    private NFCManager nfcManager;
    private final Context context = this;

    private StatesNFC stateRead;
    private PendingIntent pendingIntent;
    private IntentFilter[] writeTagFilters;

    private UserInfoGateway userInfoGateway;

    private List<Order> orders;
    private static String SAVED_INSTANCE = "SAVED_INSTANCE_LMQ_ADMIN";

    // Views
    private ImageView imageView;
    private TextView txtWaiting;
    private ProgressBar loadingMask;
    private AlertDialog bringDialog;
    private ImageView imageHand;
    private ImageButton btnRefresh;

    //Display full screen
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            imageView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    private DialogInterface.OnCancelListener cancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
            hide();
        }
    };

    /* region CONSTRUCTOR */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //init views
        this.initView(false);

        this.restoreSavedInstance(savedInstanceState);
    }

    private void initView(boolean isRotateScreen) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN); //TO NO SHOW CONTROLS
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.fullscreen_content);
        txtWaiting = findViewById(R.id.waiting_target);
        loadingMask = findViewById(R.id.loadingMask);
        imageHand = findViewById(R.id.imageHand);
        btnRefresh = findViewById(R.id.btnRefresh);

        mVisible = true;
        orders = new ArrayList<>();
        userInfoGateway = new UserInfoGateway(this, bringDialog);

        if(!isRotateScreen) {
            setMessageNfc(STATE_BUILDING);
        } else {
            endInitialLoading();
        }

        //init nfc objects
        nfcManager = new NFCManager(this, context);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };

        //EVENTS
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mVisible) {
                    hide();
                }
            }
        });
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                toggle();
                return true;
            }
        });

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateRead == STATE_WAITING_NFC) {
                    userInfoGateway.callGetOrders();
                }
            }
        });
    }

    private void restoreSavedInstance(Bundle savedInstanceState) {
        SharedDataPref sharedDataPref = null;
        if (savedInstanceState != null) {
            sharedDataPref = savedInstanceState.getParcelable(SAVED_INSTANCE);
        }

        if (savedInstanceState == null || sharedDataPref == null) {
            //load users and orders
            userInfoGateway.callGetOrders();
        } else {
            sharedDataPref = savedInstanceState.getParcelable(SAVED_INSTANCE);
            if (sharedDataPref != null) {
                orders.addAll(sharedDataPref.getOrders());
                endInitialLoading();
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(50);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        SharedDataPref sharedDataPref = new SharedDataPref();
        sharedDataPref.setOrders(orders);
        outState.putParcelable(SAVED_INSTANCE, sharedDataPref);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (stateRead == STATE_WAITING_NFC) {
            setMessageNfc(STATE_READING_NFC);
            try {
                String linkedFullName = readCard(intent);

                if (linkedFullName.equals(Constants.EMPTY_TARGET)) {
                    Utils.showAlert(context, null, R.string.empty_target, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            hide();
                            setMessageNfc(STATE_WAITING_NFC);
                        }
                    });
                    return;
                }

                if (linkedFullName.equals(Constants.ER_NULL_POINTER_READ) || linkedFullName.equals(Constants.DATA_FROM_OUTSIDE)) {
                    setMessageNfc(STATE_WAITING_NFC);
                    return;
                }

                String [] fullName = deleteLinkNFC(linkedFullName);
                if (fullName != null) {
                    userInfoGateway.setOrders(orders);
                    userInfoGateway.callGetUser(fullName);
                }
            } catch (Exception ex) {
                setMessageNfc(STATE_WAITING_NFC);
                if (bringDialog != null) {
                    bringDialog.cancel();
                }
                bringDialog = Utils.showAlert(context, R.string.error_title, R.string.er_reading, cancelListener);
            }
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        if (nfcManager == null) {
            nfcManager = new NFCManager(MainActivity.this, context);
        }
        nfcManager.disableForegroundDispatch();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!fromThisActivity) {
            show();
            hide();
        }
        fromThisActivity = false;
    }

    @Override
    public void onResume(){
        super.onResume();

        //reset user info gateway
        userInfoGateway = new UserInfoGateway(this, bringDialog);
        userInfoGateway.setOrders(orders);

        if (stateRead != STATE_CALL_RF && stateRead != STATE_CALL_RF_2 && stateRead != STATE_READING_NFC) {
            setMessageNfc(STATE_BUILDING);
        }
        String[][] techList = new String[][]{{android.nfc.tech.Ndef.class.getName()}, {android.nfc.tech.NdefFormatable.class.getName()}};

        try {
            if (nfcManager == null) {
                nfcManager = new NFCManager(MainActivity.this, context);
            }

            nfcManager.verifyNFC();
            nfcManager.enableForegroundDispatch(pendingIntent, writeTagFilters, techList);
            if (stateRead != STATE_CALL_RF && stateRead != STATE_CALL_RF_2 && stateRead != STATE_READING_NFC) {
                endInitialLoading();
            }
        } catch (NFCManager.NFCNotSupported nfcNotSupported) {
            Utils.showAlert(context, R.string.error_title, nfcNotSupported.getMessage(), cancelListener);
        } catch (NFCManager.NFCNotEnabled nfcNotEnabled) {
            Utils.showAlert(context, R.string.error_title, nfcNotEnabled.getMessage(), cancelListener);
        } catch (Exception ex) {
            Utils.showAlert(context, R.string.error_title, ex.getMessage(), cancelListener);
        }
    }
    /* endregion CONSTRUCTOR */

    /* region EVENTS */

    /* endregion EVENTS */

    /* region private METHODS */
    /**
     * Show progress bar
     */
    private void toggleVisibilitySpinner(boolean isVisible) {
        final boolean auxIsVisible = isVisible;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingMask.setVisibility(auxIsVisible ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    private void toggleVisibilityHand(boolean isVisible) {
        final boolean auxIsVisible = isVisible;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageHand.setVisibility(auxIsVisible ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    private void toggleVisibilityRefresh(boolean isVisible) {
        final boolean auxIsVisible = isVisible;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnRefresh.setVisibility(auxIsVisible ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    /**
     * Delete LINK_NFC if exists or show an error
     * @param fullName Full name formatted with Constants.LINK_NFC
     * @return String [] [0] = name and [1] = lastName
     */
    public String[] deleteLinkNFC(String fullName) {
        if (!fullName.contains(Constants.LINK_NFC)) {
            Utils.showAlert(context, R.string.error_title, R.string.er_user_not_found_by_phone, cancelListener);
            return null;
        }

        int index = fullName.indexOf(Constants.LINK_NFC);
        int endIndex = fullName.lastIndexOf(Constants.LINK_NFC) + Constants.LINK_NFC.length();
        String name = fullName.substring(0, index);
        String lastName = fullName.substring(endIndex);

        return new String[] { name, lastName};
    }

    /**
     * Read data from card
     * @param intent
     * @return
     */
    private String readCard(Intent intent) {
        try {
            return nfcManager.readFromIntent(intent);
        } catch (IOException e) {
            Utils.showAlert(context, R.string.error_title, R.string.er_reading, cancelListener);
            return Constants.ER_NULL_POINTER_READ;
        } catch (Exception e) { //have data but no from this system
            Utils.showAlert(context, R.string.error_title, R.string.er_user_not_found_by_phone, cancelListener);
            return Constants.DATA_FROM_OUTSIDE;
        }
    }

    //DISPLAY METHODS USED TO SET FULL SCREEN
    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        imageView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
    /* endregion private METHODS */

    /* region public METHODS */
    /**
     * Set message by state
     * @param state defined at top of the class
     */
    public void setMessageNfc (StatesNFC state) {
        txtWaiting.setText("");

        switch (state) {
            case STATE_BUILDING:
                stateRead = STATE_BUILDING;
                txtWaiting.setText(R.string.building_nfc);
                toggleVisibilitySpinner(true);
                toggleVisibilityHand(false);
                toggleVisibilityRefresh(false);
                break;
            case STATE_WAITING_NFC:
                stateRead = STATE_WAITING_NFC;
                txtWaiting.setText(R.string.waiting_target);
                toggleVisibilitySpinner(false);
                toggleVisibilityHand(true);
                toggleVisibilityRefresh(true);
                break;
            case STATE_READING_NFC:
                stateRead = STATE_READING_NFC;
                txtWaiting.setText(R.string.reading_nfc);
                toggleVisibilitySpinner(true);
                toggleVisibilityHand(false);
                toggleVisibilityRefresh(false);
                break;
            case STATE_CALL_RF:
                stateRead = STATE_CALL_RF;
                txtWaiting.setText(R.string.rf_call);
                toggleVisibilitySpinner(true);
                toggleVisibilityHand(false);
                toggleVisibilityRefresh(false);
                break;
            case STATE_CALL_RF_2:
                stateRead = STATE_CALL_RF_2;
                txtWaiting.setText(R.string.rf_call);
                toggleVisibilitySpinner(true);
                toggleVisibilityHand(false);
                toggleVisibilityRefresh(false);
                break;
            default:
                state = null;
                break;
        }
    }

    public void setMessageNfc (StatesNFC state, String optionalText) {
        txtWaiting.setText("");
        switch (state) {
            case STATE_BUILDING:
                stateRead = STATE_BUILDING;
                txtWaiting.setText(optionalText);
                toggleVisibilitySpinner(true);
                toggleVisibilityHand(false);
                toggleVisibilityRefresh(false);
                break;
            case STATE_WAITING_NFC:
                stateRead = STATE_WAITING_NFC;
                txtWaiting.setText(optionalText);
                toggleVisibilitySpinner(false);
                toggleVisibilityHand(true);
                toggleVisibilityRefresh(true);
                break;
            case STATE_READING_NFC:
                stateRead = STATE_READING_NFC;
                txtWaiting.setText(optionalText);
                toggleVisibilitySpinner(true);
                toggleVisibilityHand(false);
                toggleVisibilityRefresh(false);
                break;
            case STATE_CALL_RF:
                stateRead = STATE_CALL_RF;
                txtWaiting.setText(optionalText);
                toggleVisibilitySpinner(true);
                toggleVisibilityHand(false);
                toggleVisibilityRefresh(false);
                break;
            case STATE_CALL_RF_2:
                stateRead = STATE_CALL_RF_2;
                txtWaiting.setText(optionalText);
                toggleVisibilitySpinner(true);
                toggleVisibilityHand(false);
                toggleVisibilityRefresh(false);
                break;
            default:
                state = null;
                break;
        }
    }

    public void endInitialLoading() {
        if (orders != null && !orders.isEmpty()) {
            setMessageNfc(STATE_WAITING_NFC);
        }
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Context getContext() {
        return this.context;
    }

    public DialogInterface.OnCancelListener getCancelListener() {
        return this.cancelListener;
    }

    public StatesNFC getStateRead() {
        return this.stateRead;
    }

    public void startActivityUserInfo(UserInfoModel userInfoModel) {
        setMessageNfc(STATE_BUILDING);
        Intent intent = new Intent(MainActivity.this, UserInfoActivity.class);
        intent.putExtra(Constants.EXTRA_USER_INFO, userInfoModel);
        MainActivity.this.startActivity(intent);
    }
    /* endregion public METHODS */
}
