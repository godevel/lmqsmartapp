package com.godevel.lmqsmartapp.enums;

public enum StatesNFC {
    STATE_WAITING_NFC,
    STATE_READING_NFC,
    STATE_BUILDING,
    STATE_CALL_RF,
    STATE_CALL_RF_2;
}
