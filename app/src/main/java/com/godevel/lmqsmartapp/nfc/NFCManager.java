package com.godevel.lmqsmartapp.nfc;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Parcelable;

import com.godevel.lmqsmartapp.R;
import com.godevel.lmqsmartapp.utils.AES;
import com.godevel.lmqsmartapp.utils.Constants;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class NFCManager {

    private Activity activity;
    private NfcAdapter nfcAdpt;
    private Context context;

    public NFCManager(Activity activity, Context context) {
        this.activity = activity;
        this.context = context;
    }

    public void verifyNFC() throws NFCNotSupported, NFCNotEnabled {
        nfcAdpt = NfcAdapter.getDefaultAdapter(activity);

        if (nfcAdpt == null) {
            throw new NFCNotSupported();
        }

        if (!nfcAdpt.isEnabled()) {
            throw new NFCNotEnabled();
        }
    }

    /**
     * Enable foreground dispatch
     * @param pendingIntent
     * @param writeTagFilters
     * @param techList
     */
    public void enableForegroundDispatch(PendingIntent pendingIntent, IntentFilter[] writeTagFilters, String[][] techList){
        if (nfcAdpt != null) {
            nfcAdpt.enableForegroundDispatch(activity, pendingIntent, writeTagFilters, techList);
        }
    }

    /**
     * Disable foreground dispatch
     */
    public void disableForegroundDispatch(){
        if (nfcAdpt != null) {
            nfcAdpt.disableForegroundDispatch(activity);
        }
    }

    /* READ METHODS */
    public String readFromIntent(Intent intent) throws IOException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            return buildTagViews(msgs);
        }
        return null;
    }

    private String buildTagViews(NdefMessage[] msgs) throws IOException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        if (msgs == null || msgs.length == 0) return Constants.EMPTY_TARGET;

        String text = "";
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"

        // Get the Text
        text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        return AES.decrypt(text);
    }

    /* EXCEPTION CLASSES */
    public class NFCNotSupported extends Exception {
        public NFCNotSupported() {
            super(context.getResources().getString(R.string.er_not_sorported));
        }
    }

    public class NFCNotEnabled extends Exception {
        public NFCNotEnabled() {
            super(context.getResources().getString(R.string.er_not_enabled));
        }
    }

}
