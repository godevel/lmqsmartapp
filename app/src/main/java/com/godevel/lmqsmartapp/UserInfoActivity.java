package com.godevel.lmqsmartapp;

import android.annotation.SuppressLint;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.godevel.lmqsmartapp.models.Appointment;
import com.godevel.lmqsmartapp.models.UserInfoModel;
import com.godevel.lmqsmartapp.utils.Constants;

import java.text.ParseException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class UserInfoActivity extends AppCompatActivity {
    private UserInfoModel userInfoModel;
    private final Context context = this;

    //VIEWS
    private Button btnClose;
    private TextView txtDay1;
    private TextView txtDay2;
    private TextView txtDay3;

    private TextView txtScheduledNum;
    private TextView txtNoScheduledNum;

    private TextView txtNextRevisionDate;
    private TextView txtFrequency;

    private TextView txtPayment;

    private TextView txtStartTreatmentDate;
    private TextView txtStartTreatmentDays;

    private TextView txtRevisionGoals;
    private TextView txtRevisionGoalsTitle;

    private TextView txtHistory;
    private TextView txtHistoryLast;

    static Timer timer;
    //PROPERTIES FULL SCREEN
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.initView();
    }

    private void initView() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN); //TO NO SHOW CONTROLS

        setContentView(R.layout.activity_user_info);

        //init views:
        btnClose = findViewById(R.id.btnClose);
        txtDay1 = findViewById(R.id.txtDay1);
        txtDay2 = findViewById(R.id.txtDay2);
        txtDay3 = findViewById(R.id.txtDay3);

        txtScheduledNum = findViewById(R.id.txtScheduled);
        txtNoScheduledNum = findViewById(R.id.txtNoScheduledNum);

        txtNextRevisionDate = findViewById(R.id.txtNextRevisionDate);
        txtFrequency = findViewById(R.id.txtFrequency);

        txtPayment = findViewById(R.id.txtPayment);

        txtStartTreatmentDate = findViewById(R.id.txtStartTreatmentDate);
        txtStartTreatmentDays = findViewById(R.id.txtStartTreatmentDays);

        txtRevisionGoals = findViewById(R.id.txtRevisionGoals);
        txtRevisionGoalsTitle = findViewById(R.id.txtRevisionGoalsTitle);

        txtHistory = findViewById(R.id.txtHistory);
        txtHistoryLast = findViewById(R.id.txtHistoryLast);

        mVisible = true;
        mContentView = findViewById(R.id.imageView);

        this.loadData();

        //init timeout
        initTimeout();

        //INIT EVENTS
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickCloseBtn();
            }
        });
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mVisible) {
                    hide();
                }
            }
        });
        mContentView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                toggle();
                return true;
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        this.initView();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(0);
    }

    /* EVENTS */
    private void onClickCloseBtn() {
        timer.cancel();
        finish();
    }

    /* region METHODS */
    private void loadData() {
        userInfoModel = (UserInfoModel) getIntent().getSerializableExtra(Constants.EXTRA_USER_INFO);

        paintDays();
        paintNextVisits();
        paintNoScheduledVisits();
        boolean hasRevision = paintNextRevision();
        paintNextPayment(hasRevision);
        paintStartTreatmentDate();
        paintRevisionGoals();
        paintHistory();
    }

    private void paintDays() {
        if (userInfoModel == null) {
            txtDay1.setVisibility(View.INVISIBLE);
            txtDay2.setVisibility(View.INVISIBLE);
            txtDay3.setVisibility(View.INVISIBLE);

            return;
        }
        List<Appointment> nextTreeVisits = userInfoModel.getNextTreeVisits();

        if (nextTreeVisits == null || nextTreeVisits.isEmpty()) {
            txtDay1.setText(context.getResources().getString(R.string.txtNoAdjustments));
            txtDay2.setVisibility(View.INVISIBLE);
            txtDay3.setVisibility(View.INVISIBLE);
            return;
        }

        if (nextTreeVisits.size() > 0) {
            txtDay1.setText(nextTreeVisits.get(0).getDate() + " - " + nextTreeVisits.get(0).getTime());
        } else {
            txtDay1.setVisibility(View.INVISIBLE);
        }

        if (nextTreeVisits.size() > 1) {
            txtDay2.setText(nextTreeVisits.get(1).getDate() + " - " + nextTreeVisits.get(1).getTime());
        } else {
            txtDay2.setVisibility(View.INVISIBLE);
        }

        if (nextTreeVisits.size() > 2) {
            txtDay3.setText(nextTreeVisits.get(2).getDate() + " - " + nextTreeVisits.get(2).getTime());
        } else {
            txtDay3.setVisibility(View.INVISIBLE);
        }
    }

    private void paintNextVisits() {
        String msg = "";
        if (userInfoModel.getNextRevision() == null || userInfoModel.getNextRevision().equals("")) {
            msg = String.valueOf(context.getResources().getString(R.string.txtScheduledNoRevision));
        } else {
            msg = String.valueOf(context.getResources().getString(R.string.txtScheduled));
        }

        if (userInfoModel == null) {
            txtScheduledNum.setText("0 " + msg);
            return;
        }

        txtScheduledNum.setText(userInfoModel.getScheduledVisits() + " " + msg);
    }

    private void paintNoScheduledVisits() {
        if (userInfoModel == null) {
            txtNoScheduledNum.setText(String.valueOf("0 " + context.getResources().getString(R.string.txtNoScheduledNum)));
            return;
        }

        txtNoScheduledNum.setText(String.valueOf(userInfoModel.getNoScheduledVisits()) + " " + context.getResources().getString(R.string.txtNoScheduledNum));
    }

    private boolean paintNextRevision() {
        boolean hasNextRevision = false;
        if (userInfoModel == null) {
            return false;
        }

        if (userInfoModel.getNextRevision() == null || userInfoModel.getNextRevision().equals("")) {
            txtNextRevisionDate.setText(context.getResources().getString(R.string.txtNoScheduled));
        } else {
            txtNextRevisionDate.setText(userInfoModel.getNextRevision().getDate() + " - " + userInfoModel.getNextRevision().getTime());
            hasNextRevision = true;
        }

        if (userInfoModel.getFrequency() == null || userInfoModel.getFrequency().equals("")) {
            txtFrequency.setText("");
        } else {
            txtFrequency.setText(context.getResources().getString(R.string.txtCurrentFrequency).replace("{freq}", userInfoModel.getFrequency()));
        }

        return hasNextRevision;
    }

    private void paintNextPayment(boolean hasRevision) {
        if (userInfoModel == null || userInfoModel.getPaymentInfo() == null || userInfoModel.getPaymentInfo().equals("")) {
            txtPayment.setText(context.getResources().getString(R.string.noPaymentInfo));
            return;
        }

        txtPayment.setText(userInfoModel.getPaymentInfo());
    }

    private void paintStartTreatmentDate() {
        if (userInfoModel == null) {
            txtStartTreatmentDate.setText("");
            txtStartTreatmentDays.setText("");
            return;
        }

        txtStartTreatmentDate.setText(userInfoModel.getFirstAdjustment());
        try {
            long days = userInfoModel.getNumberDaysFromFirstAdjustment();
            if (days > 0) {
                txtStartTreatmentDays.setText(context.getResources().getString(R.string.startTreatmentDays).replace("{days}", String.valueOf(days)));
            } else {
                txtStartTreatmentDays.setText("");
            }
        } catch (ParseException e) {
            txtStartTreatmentDays.setText("");
        }
    }

    private void paintRevisionGoals() {
        if (userInfoModel == null || userInfoModel.getRevisionGoals() == null || userInfoModel.getRevisionGoals().equals("")) {
            txtRevisionGoalsTitle.setVisibility(View.INVISIBLE);
            txtRevisionGoals.setText("");
            return;
        }

        txtRevisionGoals.setText(userInfoModel.getRevisionGoals());
    }

    private void paintHistory() {
        if (userInfoModel == null) {
            txtHistory.setText("");
            txtHistoryLast.setText("");
            return;
        }

        if (userInfoModel.getHistoryAdjustments() > 0) {
            txtHistory.setText(userInfoModel.getHistoryAdjustments() + " " + context.getResources().getString(R.string.historyAdjustments));
        }

        try {
            long lastAdjustment = userInfoModel.getNumberDaysFromLastAdjustment();
            if (lastAdjustment == 0) {
                txtHistoryLast.setText(context.getResources().getString(R.string.lastAdjustmentToday));
            } else if (lastAdjustment > 0) {
                txtHistoryLast.setText(context.getResources().getString(R.string.lastAdjustment).replace("{days}", String.valueOf(lastAdjustment)));
            }
        } catch (ParseException e) {
            txtHistoryLast.setText("");
        }
    }
    /* FULLSCREEN METHODS */
    private void initTimeout() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                finish();
            }
        }, 120000, 120000);
    }
    //METHODS FULL SCREEN
    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
