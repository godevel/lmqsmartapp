package com.godevel.lmqsmartapp.utils;

import com.godevel.lmqsmartapp.models.AppointmentType;

public class Constants {
    //Extras - Activity change
    public static final String EXTRA_USER_INFO = "EXTRA_USER_INFO";

    //API ACUITY USER KEY
    public static final String API_USER = "19192915";
    public static final String API_KEY = "9f62967ff14b9f878718e65424314334";
    //API URIS
    public static final String BASE_API_URL = "https://acuityscheduling.com/api/";
    //API ENDPOINTS
    public static final String GET_USER = "v1/clients";
    public static final String GET_NEXT_VISIT = "v1/appointments";
    public static final String CHECK_CERTIFICATES = "v1/certificates/check";
    public static final String GET_CERTIFICATES = "v1/certificates";
    public static final String GET_ORDERS = "v1/orders";

    //SECRETS ENCRIPTATION
    public static final String SECRET_KEY = "bT$=9jVLu^D+X%IR6VMxZVB+ERf6t=(Agst#+|kC!|<xENKL-DBo-KRg=bDl%qc";
    public static String LINK_NFC = "**LINK**";
    //MESSAGES
    public static final String DATA_FROM_OUTSIDE = "+LMQ";
    public static final String ER_NULL_POINTER_READ = "ERROR NULL POINTER ON READ";
    public static final String EMPTY_TARGET = "EMPTY_TARGET";

    public static final int NUMBER_NEXT_VISITS = 3;

    //APPOINTMENT TYPES
    public static final AppointmentType FIRST_VISIT = new AppointmentType(7514495);
    public static final AppointmentType INITIAL_INFO = new AppointmentType(7514532);
    public static final AppointmentType AT_R1 = new AppointmentType(10478851);
    public static final AppointmentType AT_R2 = new AppointmentType(7514587);
    public static final AppointmentType AT_R3 = new AppointmentType(10699807);
    public static final AppointmentType ADJUSTMENT = new AppointmentType(7536963);

    //FORM FIELDS
    public static final int ID_BASIC_FORM = 997453;
    public static final int FIELD_ID_FIELD_NEXT_PAYMENT = 5551243;

    public static final int ID_REVISION_PLAN_FORM = 997691;
    public static final int FIELD_ID_NEXT_META = 5347937;
    public static final int FIELD_ID_FREQUENCY = 5672184;

    public static final int ID_FIRS_REV_PLAN_FORM = 997647;
    public static final int FIELD_ID_FIRS_REV_NEXT_META = 5347608;
    public static final int FIELD_ID_FIRS_REV_FREQUENCY = 7749433;
}
