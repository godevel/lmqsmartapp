package com.godevel.lmqsmartapp.utils;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.godevel.lmqsmartapp.R;
import com.godevel.lmqsmartapp.models.Appointment;

import java.util.Arrays;

public class Utils {

    public static boolean contains(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }

    public static boolean isTypeRevision(Integer appointmentType) {
        return appointmentType.intValue() == Constants.AT_R1.getId().intValue() || appointmentType.intValue() == Constants.AT_R2.getId().intValue()
                || appointmentType.intValue() == Constants.AT_R3.getId().intValue();
    }

    //DIALOGS
    public static AlertDialog showAlert(Context context, int title, int message, DialogInterface.OnCancelListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, getStyleId(null, context));
        builder.setTitle(context.getResources().getString(title))
                .setMessage(context.getResources().getString(message))
                .setCancelable(true)
                .setOnCancelListener(cancelListener)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showAlert(Context context, int title, String message, DialogInterface.OnCancelListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, getStyleId(null, context));
        builder.setTitle(context.getResources().getString(title))
                .setMessage(message)
                .setCancelable(true)
                .setOnCancelListener(cancelListener)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showAlert(Context context, String title, String message, DialogInterface.OnCancelListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, getStyleId(null, context));
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setOnCancelListener(cancelListener)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showAlert(Context context, String title, int message, DialogInterface.OnCancelListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, getStyleId(null, context));
        builder.setTitle(title)
                .setMessage(context.getResources().getString(message))
                .setCancelable(true)
                .setOnCancelListener(cancelListener)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static int getStyleId(String title, Context context) {
        int id = 0;
        if (title == null) {
            id = context.getResources().getIdentifier("Theme_Custom_Dialog_Alert_No_Title", "style", context.getPackageName());
        } else {
            id = context.getResources().getIdentifier("Theme_Custom_Dialog_Alert", "style", context.getPackageName());
        }

        return id;
    }
}
