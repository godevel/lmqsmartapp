package com.godevel.lmqsmartapp.gateway;

import androidx.appcompat.app.AlertDialog;

import com.godevel.lmqsmartapp.MainActivity;
import com.godevel.lmqsmartapp.R;
import com.godevel.lmqsmartapp.models.Appointment;
import com.godevel.lmqsmartapp.models.AppointmentType;
import com.godevel.lmqsmartapp.models.Certificate;
import com.godevel.lmqsmartapp.models.Field;
import com.godevel.lmqsmartapp.models.Form;
import com.godevel.lmqsmartapp.models.Order;
import com.godevel.lmqsmartapp.models.User;
import com.godevel.lmqsmartapp.models.UserInfoModel;
import com.godevel.lmqsmartapp.utils.Constants;
import com.godevel.lmqsmartapp.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_BUILDING;
import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_CALL_RF;
import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_CALL_RF_2;
import static com.godevel.lmqsmartapp.enums.StatesNFC.STATE_WAITING_NFC;

public class UserInfoGateway {

    private final int FROM_GET_OLD = 0;
    private final int FROM_GET_NEXT = 1;

    private MainActivity mainActivity;
    private AlertDialog bringDialog;
    private int pendingLoops;
    private UserInfoModel userInfoModel;
    private List<Order> orders;

    public UserInfoGateway(MainActivity mainActivity, AlertDialog bringDialog) {
        this.mainActivity = mainActivity;
        this.bringDialog = bringDialog;
        this.pendingLoops = 0;
        this.userInfoModel = new UserInfoModel();
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    /* region CALLS */
    public void callGetUser(final String[] fullName) {
        mainActivity.setMessageNfc(STATE_CALL_RF);
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<User>> call = gateway.getAcuitySchedulingInterface().getUser(gateway.getBasicAuthenticationKey(), fullName[0], fullName[1]);

        call.enqueue(new Callback<List<User>>() {

            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<User> users = response.body();
                if (users == null || users.size() == 0) {
                    Utils.showAlert(mainActivity.getContext(), R.string.alert_title, R.string.er_user_not_found, mainActivity.getCancelListener());
                    mainActivity.setMessageNfc(STATE_WAITING_NFC);
                    return;
                }else if (users.size() > 1) {
                    String usersList = "";
                    List<User> auxUsers = new ArrayList<>();
                    //segunda comprobación, buscar más de un usuario con mismo nombre y apellido
                    for(User user : users) {
                        if (user.getFirstName().equals(fullName[0]) && user.getLastName().equals(fullName[1])) {
                            auxUsers.add(user);
                        }
                    }
                    if (auxUsers.size() > 1) {
                        for (User user : auxUsers) {
                            usersList += "\n- " + user.getFullName();
                        }
                        Utils.showAlert(mainActivity.getContext(), R.string.error_title, R.string.er_more_than_one_user + usersList, mainActivity.getCancelListener());
                        mainActivity.setMessageNfc(STATE_WAITING_NFC);
                        return;
                    }

                    users = auxUsers;
                }

                //save info for next activity
                userInfoModel.setUser(users.get(0));
                callGetPaymentInfo();
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * download payment info
     */
    private void callGetPaymentInfo() {
        User user = userInfoModel.getUser();
        mainActivity.setMessageNfc(STATE_CALL_RF_2, mainActivity.getContext().getResources().getString(R.string.rf_call_2).replace("{name}", user.getFirstName()));
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Appointment>> call = gateway.getAcuitySchedulingInterface().getAppointmentByType(gateway.getBasicAuthenticationKey(), user.getFirstName(), user.getLastName(), Constants.FIRST_VISIT.getId());

        call.enqueue(new Callback<List<Appointment>>() {
            @Override
            public void onResponse(Call<List<Appointment>> call, Response<List<Appointment>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<Appointment> appointments = response.body();

                if (appointments != null && !appointments.isEmpty()) {
                    List<Form> forms = appointments.get(0).getForms();
                    for(Form form : forms) {
                        if (form.getId() == Constants.ID_BASIC_FORM){
                            for(Field field : form.getValues()) {
                                if (field.getFieldID() == Constants.FIELD_ID_FIELD_NEXT_PAYMENT) {
                                    userInfoModel.setPaymentInfo(field.getValue());
                                }
                            }
                        }
                    }
                }

                callGetOldAppointments();
            }

            @Override
            public void onFailure(Call<List<Appointment>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * download old appointments
     */
    private void callGetOldAppointments() {
        User user = userInfoModel.getUser();
        mainActivity.setMessageNfc(STATE_CALL_RF_2, mainActivity.getContext().getResources().getString(R.string.rf_call_2).replace("{name}", user.getFirstName()));
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Appointment>> call = gateway.getAcuitySchedulingInterface().getOldVisits(gateway.getBasicAuthenticationKey(), user.getFirstName(), user.getLastName(), getToday(), "ASC");

        call.enqueue(new Callback<List<Appointment>>() {

            @Override
            public void onResponse(Call<List<Appointment>> call, Response<List<Appointment>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<Appointment> auxAppointments = response.body();
                List<Appointment> oldAppointments = new ArrayList<>();
                int indexRevision = -1;

                for (int i = 0; i < auxAppointments.size(); i++) {
                    Appointment aux = auxAppointments.get(i);
                    if(aux.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()){
                        oldAppointments.add(auxAppointments.get(i));
                    }

                    if (aux.getAppointmentTypeID().intValue() == Constants.INITIAL_INFO.getId().intValue() || Utils.isTypeRevision(aux.getAppointmentTypeID())) {
                        indexRevision = i;
                    }
                }

                //save form goals and frequency
                if (indexRevision != -1) {
                    saveGoals(auxAppointments, indexRevision);
                }

                //save info for next activity
                userInfoModel.setOldAppointments(oldAppointments);

                callGetNextAppointments();
            }

            @Override
            public void onFailure(Call<List<Appointment>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * download next appointments
     */
    private void callGetNextAppointments() {
        User user = userInfoModel.getUser();
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Appointment>> call = gateway.getAcuitySchedulingInterface().getNextVisits(gateway.getBasicAuthenticationKey(), user.getFirstName(), user.getLastName(), getToday(), "ASC");

        call.enqueue(new Callback<List<Appointment>>() {

            @Override
            public void onResponse(Call<List<Appointment>> call, Response<List<Appointment>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<Appointment> auxAppointments = response.body();
                List<Appointment> nextTreeAppointments = new ArrayList<>();
                int indexRevision = -1;

                List<Appointment> nextAppointmentsToNextRevision = new ArrayList<>();

                //get next tree visits
                if (!auxAppointments.isEmpty()) {
                    for (int i = 0; i < Constants.NUMBER_NEXT_VISITS; i++) {
                        if (i < auxAppointments.size()) {
                            Appointment aux = auxAppointments.get(i);
                            if (aux.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()) {
                                nextTreeAppointments.add(auxAppointments.get(i));
                            }
                        }
                    }
                }

                //get next adjustment appointments to the next visit and next revision
                for (int i = 0; i < auxAppointments.size(); i++) {
                    Appointment aux = auxAppointments.get(i);

                    if (aux.getAppointmentTypeID().intValue() == Constants.ADJUSTMENT.getId().intValue()) {
                        nextAppointmentsToNextRevision.add((auxAppointments.get(i)));
                    }
                    if (Utils.isTypeRevision(aux.getAppointmentTypeID())){
                        AppointmentType nextRevision = new AppointmentType(
                                aux.getId(), aux.getType(), aux.getDate(),
                                aux.getTime(), aux.getEndTime(), aux.getDatetime());

                        userInfoModel.setNextRevision(nextRevision);

                        break;
                    }
                    if (aux.getAppointmentTypeID().intValue() == Constants.INITIAL_INFO.getId().intValue()) {
                        indexRevision = i;
                    }
                }

                //save info for next activity
                userInfoModel.setNextTreeVisits(nextTreeAppointments);
                userInfoModel.setNextAppointmentsToNextRevision(nextAppointmentsToNextRevision);

                //get sale orders
                loadOrders();
            }

            @Override
            public void onFailure(Call<List<Appointment>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * load sale orders and call to get certificates
     */
    private void loadOrders() {
        User user = userInfoModel.getUser();
        List<Order> auxOrders = new ArrayList<>();

        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                if (order.getFirstName().equals(user.getFirstName()) && order.getLastName().equals(user.getLastName())) {
                    auxOrders.add(order);
                }
            }
        }

        userInfoModel.setOrders(auxOrders);

        if (auxOrders != null && auxOrders.size() > 0) {
            pendingLoops = auxOrders.size();
            for (Order order : auxOrders) {
                getCertificates(order.getId());
            }
        } else {
            mainActivity.startActivityUserInfo(userInfoModel);
        }
    }

    /**
     * Public get user orders
     * Used from MainActivity to load order configurations
     */
    public void callGetOrders() {
        mainActivity.setMessageNfc(STATE_BUILDING, mainActivity.getContext().getResources().getString(R.string.rf_call_order));
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Order>> call = gateway.getAcuitySchedulingInterface().getOrders(gateway.getBasicAuthenticationKey(), 2147483647);

        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<Order> orders = response.body();
                mainActivity.setOrders(response.body());
                mainActivity.endInitialLoading();
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                onFailureAction(R.string.er_get_orders);
            }
        });
    }

    /**
     * Get certificate
     * @param orderId
     */
    private void getCertificates(int orderId) {
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Certificate>> call = gateway.getAcuitySchedulingInterface().getCertificates(gateway.getBasicAuthenticationKey(), orderId);

        call.enqueue(new Callback<List<Certificate>>() {

            @Override
            public void onResponse(Call<List<Certificate>> call, Response<List<Certificate>> response) {
                if (mainActivity.getStateRead() != STATE_CALL_RF_2) { return; }
                if (response.code() == 200) {
                    //save certificate
                    for (Certificate certificate : response.body())
                    {
                        userInfoModel.addCertificate(certificate);
                    }
                }

                //exit when finish
                pendingLoops--;
                if (pendingLoops == 0) {
                    mainActivity.startActivityUserInfo(userInfoModel);
                }
            }

            @Override
            public void onFailure(Call<List<Certificate>> call, Throwable t) {
                onFailureAction();
            }
        });
    }

    /**
     * get certificate
     * @param certificate
     * @param appointmentTypeID
     * @param origin
     */
    private void callCheckCertificate(String certificate, Integer appointmentTypeID, int origin) {
        final int auxOrigin = origin;
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<Certificate> call = gateway.getAcuitySchedulingInterface().checkCertificate(gateway.getBasicAuthenticationKey(), certificate, appointmentTypeID);

        call.enqueue(new Callback<Certificate>() {

            @Override
            public void onResponse(Call<Certificate> call, Response<Certificate> response) {
                if (mainActivity.getStateRead() != STATE_CALL_RF_2) { return; }
                if (response.code() == 200) {
                    //save certificate
                    userInfoModel.addCertificate(response.body());
                }

                switch (auxOrigin){
                    case FROM_GET_OLD:
                        //exit when finish
                        pendingLoops--;
                        if (pendingLoops == 0) {
                            callGetNextAppointments();
                        }
                        break;
                    case FROM_GET_NEXT:
                        //exit when finish
                        pendingLoops--;
                        if (pendingLoops == 0) {
                            mainActivity.startActivityUserInfo(userInfoModel);
                        }
                        break;
                }
            }

            @Override
            public void onFailure(Call<Certificate> call, Throwable t) {
                pendingLoops = 0;
                if (bringDialog != null) {
                    bringDialog.cancel();
                }
                bringDialog = Utils.showAlert(mainActivity.getContext(), R.string.error_title, R.string.er_rf, mainActivity.getCancelListener());
                mainActivity.setMessageNfc(STATE_WAITING_NFC);
            }
        });
    }

    /**
     * get next order to pay
     */
    private void callGetNextOrderToPay() {
        final User auxUser = userInfoModel.getUser();
        mainActivity.setMessageNfc(STATE_CALL_RF_2, mainActivity.getContext().getResources().getString(R.string.rf_call_2).replace("{name}", auxUser.getFirstName()));
        AcuitySchedulingGateway gateway = new AcuitySchedulingGateway();
        Call<List<Order>> call = gateway.getAcuitySchedulingInterface().getOrders(gateway.getBasicAuthenticationKey(), null);

        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                if (errorAPIAction(response.code())) {
                    return;
                }

                List<Order> orders = response.body();
                Order auxOrder = null;

                if (orders != null && !orders.isEmpty()) {
                    for (Order order : orders) {
                        if (order.getFirstName().equals(auxUser.getFirstName()) && order.getLastName().equals(auxUser.getLastName())) {
                            if (auxOrder == null) {
                                auxOrder = order;
                            }

                            try {
                                if (auxOrder != null && auxOrder.getTimeFormatted().compareTo(order.getTimeFormatted()) > 0) {
                                    auxOrder = order;
                                }
                            } catch (ParseException pe) {}

                        }
                    }
                }

                userInfoModel.setNextOrderToPay(auxOrder);

                mainActivity.startActivityUserInfo(userInfoModel);
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                if (bringDialog != null) {
                    bringDialog.cancel();
                }
                bringDialog = Utils.showAlert(mainActivity.getContext(), R.string.error_title, R.string.er_rf, mainActivity.getCancelListener());

                mainActivity.setMessageNfc(STATE_WAITING_NFC);
            }
        });
    }
    /* endregion CALLS */

    /* region private METHODS */
    private Date getToday() {
        return new Date();
    }

    private void saveGoals(List<Appointment> appointments, int indexRevision) {
        for (Form form : appointments.get(indexRevision).getForms()) {
            if (form.getId() == Constants.ID_FIRS_REV_PLAN_FORM) {
                for (Field field : form.getValues()) {
                    if (field.getFieldID() == Constants.FIELD_ID_FIRS_REV_NEXT_META) {
                        userInfoModel.setRevisionGoals(field.getValue());
                    }
                    if (field.getFieldID() == Constants.FIELD_ID_FIRS_REV_FREQUENCY) {
                        userInfoModel.setFrequency(field.getValue());
                    }
                }
            } else if (form.getId() == Constants.ID_REVISION_PLAN_FORM) {
                for (Field field : form.getValues()) {
                    if (field.getFieldID() == Constants.FIELD_ID_NEXT_META) {
                        userInfoModel.setRevisionGoals(field.getValue());
                    }
                    if (field.getFieldID() == Constants.FIELD_ID_FREQUENCY) {
                        userInfoModel.setFrequency(field.getValue());
                    }
                }
            }
        }
    }

    private boolean errorAPIAction(int code) {
        if (code != 200) {
            if (bringDialog != null) {
                bringDialog.cancel();
            }
            bringDialog = Utils.showAlert(mainActivity.getContext(), R.string.error_title, R.string.er_rf, mainActivity.getCancelListener());

            mainActivity.setMessageNfc(STATE_WAITING_NFC);
            return true;
        }
        return false;
    }

    private void onFailureAction () {
        pendingLoops = 0;
        if (bringDialog != null) {
            bringDialog.cancel();
        }
        bringDialog = Utils.showAlert(mainActivity.getContext(), R.string.error_title, R.string.er_rf, mainActivity.getCancelListener());
        mainActivity.setMessageNfc(STATE_WAITING_NFC);
    }

    private void onFailureAction (int customMessage) {
        pendingLoops = 0;
        if (bringDialog != null) {
            bringDialog.cancel();
        }
        bringDialog = Utils.showAlert(mainActivity.getContext(), R.string.error_title, customMessage, mainActivity.getCancelListener());
        mainActivity.setMessageNfc(STATE_WAITING_NFC);
    }
    /* endregion private METHODS */
}
