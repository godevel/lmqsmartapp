package com.godevel.lmqsmartapp.gateway;

import com.godevel.lmqsmartapp.models.Appointment;
import com.godevel.lmqsmartapp.models.Order;
import com.godevel.lmqsmartapp.models.User;
import com.godevel.lmqsmartapp.models.Certificate;
import com.godevel.lmqsmartapp.utils.Constants;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface AcuitySchedulingInterface {

    /**
     * Get user by phone. Delete '+' from phone number
     * @param basicAuth Authorization id
     * @param firstName
     * @param lastName
     * @return
     */
    @GET(Constants.GET_USER)
    Call<List<User>> getUser(@Header("Authorization") String basicAuth, @Query("firstName") String firstName, @Query("lastName") String lastName);

    /**
     * get next visites by min date and ordered by minDate
     * @param basicAuth Authorization id
     * @param firstName
     * @param lastName
     * @param minDate
     * @param direction DESC|ASC
     * @return
     */
    @GET(Constants.GET_NEXT_VISIT)
    Call<List<Appointment>> getNextVisits(@Header("Authorization") String basicAuth, @Query("firstName") String firstName, @Query("lastName") String lastName, @Query("minDate") Date minDate, @Query("direction") String direction);

    @GET(Constants.GET_NEXT_VISIT)
    Call<List<Appointment>> getOldVisits(@Header("Authorization") String basicAuth, @Query("firstName") String firstName, @Query("lastName") String lastName, @Query("maxDate") Date maxDate, @Query("direction") String direction);

    @GET(Constants.CHECK_CERTIFICATES)
    Call<Certificate> checkCertificate(@Header("Authorization") String basicAuth, @Query("certificate") String certificate, @Query("appointmentTypeID") int appointmentTypeID);

    @GET(Constants.GET_CERTIFICATES)
    Call<List<Certificate>> getCertificates(@Header("Authorization") String basicAuth, @Query("orderID") int orderID);

    @GET(Constants.GET_NEXT_VISIT)
    Call<List<Appointment>> getAppointmentByType(@Header("Authorization") String basicAuth, @Query("firstName") String firstName, @Query("lastName") String lastName, @Query("appointmentTypeID") int appointmentTypeID);

    @GET(Constants.GET_ORDERS)
    Call<List<Order>> getOrders(@Header("Authorization") String basicAuth, @Query("max") Integer max);

}
